package com.bgd.gener.freemarker;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bgd.gener.bean.JavaBean;

import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;

/**
 * 处理Freemarker的工具类
 * 
 * @remark create 2016-11-18 baiguodong
 */
public final class FreeMarkerUtils
{
    /**
     * 输出控制信息
     * 
     * @throws IOException
     * @throws TemplateException
     * @remark create 2016-11-18 baiguodong
     */
    public static void out(List<JavaBean> javaBeans, String savePath) throws TemplateException, IOException
    {
        // 1.创建配置实例Cofiguration
        Configuration cfg = new Configuration(Configuration.getVersion());
        
        // 2.设置模板文件目录
        // （1）src目录下的目录（template在src下）
        // cfg.setDirectoryForTemplateLoading(new File("src/template"));
        // （2）完整路径（template在src下）
        // cfg.setDirectoryForTemplateLoading(new File(
        // "D:/cpic-env/workspace/javaFreemarker/src/template"));
        // cfg.setDirectoryForTemplateLoading(new File("src/template"));
        // （3）工程目录下的目录（template/main在工程下）--推荐
        cfg.setDirectoryForTemplateLoading(new File("src/com/bgd/gener/freemarker"));
        // cfg.setObjectWrapper(new DefaultObjectWrapper());
        // 获取模板（template）
        for (JavaBean javaBean : javaBeans)
        {
            FreeMarkerUtils.generModel(cfg, javaBean, savePath);
            FreeMarkerUtils.generMapper(cfg, javaBean, savePath);
            FreeMarkerUtils.generDao(cfg, javaBean, savePath);
            FreeMarkerUtils.generCtrl(cfg, javaBean, savePath);
            FreeMarkerUtils.generService(cfg, javaBean, savePath);
            FreeMarkerUtils.generDomain(cfg, javaBean, savePath);
        }
        
    }
    
    /**
     * 生成JavaModel对象
     * @return
     * @throws IOException 
     * @throws TemplateException 
     * @remark create 2016-11-21 baiguodong
     */
    private static void generModel(Configuration cfg, JavaBean javaBean, String savePath) throws TemplateException, IOException
    {
        Template template = cfg.getTemplate("javaBean.ftl");
        // 建立数据模型（Map）
        Map<String, Object> root = new HashMap<String, Object>();
        root.put("javaBean", javaBean);
        
        String filePath = savePath + "\\model\\";
        File file = new File(filePath);
        if (!file.exists())
        {
            file.mkdirs();
        }
        
        // 获取输出流（指定到控制台（标准输出））
        OutputStream outStream = new FileOutputStream(new File(filePath + javaBean.getBeanName() + ".java"));
        Writer out = new OutputStreamWriter(outStream); // System.out
        // StringWriter out = new StringWriter();
        // System.out.println(out.toString());
        // 数据与模板合并（数据+模板=输出）
        template.process(root, out);
        out.flush();
        
        out.close();
    }
    
    /**
     * 生成MyBatis的数据库操作文件
     * @param cfg
     * @param javaBean
     * @param savePath
     * @remark create 2016-11-21 baiguodong
     */
    private static void generMapper(Configuration cfg, JavaBean javaBean, String savePath) throws TemplateException, IOException
    {
        Template template = cfg.getTemplate("mapper.ftl");
        // 建立数据模型（Map）
        Map<String, Object> root = new HashMap<String, Object>();
        root.put("javaBean", javaBean);
        root.put("dbTableName", javaBean.getDbName());
        String filePath = savePath + "\\dao\\";
        File file = new File(filePath);
        if (!file.exists())
        {
            file.mkdirs();
        }
        
        // 获取输出流（指定到控制台（标准输出））
        OutputStream outStream = new FileOutputStream(new File(filePath + javaBean.getBeanName() + "Dao.xml"));
        Writer out = new OutputStreamWriter(outStream); // System.out
        // StringWriter out = new StringWriter();
        // System.out.println(out.toString());
        // 数据与模板合并（数据+模板=输出）
        template.process(root, out);
        out.flush();
        
        out.close();
    }
    
    /**
     * 生成到文件
     * @param cfg
     * @param javaBean
     * @param savePath
     * @param dbTableName
     * @remark create 2016-11-21 baiguodong
     */
    private static void generDao(Configuration cfg, JavaBean javaBean, String savePath) throws TemplateException, IOException
    {
        Template template = cfg.getTemplate("dao.ftl");
        // 建立数据模型（Map）
        Map<String, Object> root = new HashMap<String, Object>();
        root.put("javaBean", javaBean);
        root.put("daTableName", javaBean.getDbName());
        String filePath = savePath + "\\dao\\";
        File file = new File(filePath);
        if (!file.exists())
        {
            file.mkdirs();
        }
        
        // 获取输出流（指定到控制台（标准输出））
        OutputStream outStream = new FileOutputStream(new File(filePath + javaBean.getBeanName() + "Dao.java"));
        Writer out = new OutputStreamWriter(outStream); // System.out
        // StringWriter out = new StringWriter();
        // System.out.println(out.toString());
        // 数据与模板合并（数据+模板=输出）
        template.process(root, out);
        out.flush();
        
        out.close();
    }
    
    
    /**
     * 生成Controller
     * @param cfg
     * @param javaBean
     * @param savePath
     * @throws TemplateException
     * @throws IOException
     * @remark create 2016-11-23 baiguodong
     */
    private static void generCtrl(Configuration cfg, JavaBean javaBean, String savePath) throws TemplateException, IOException
    {
        Template template = cfg.getTemplate("controller.ftl");
        // 建立数据模型（Map）
        Map<String, Object> root = new HashMap<String, Object>();
        root.put("javaBean", javaBean);
        root.put("daTableName", javaBean.getDbName());
        String filePath = savePath + "\\controller\\";
        File file = new File(filePath);
        if (!file.exists())
        {
            file.mkdirs();
        }
        
        // 获取输出流（指定到控制台（标准输出））
        OutputStream outStream = new FileOutputStream(new File( filePath + javaBean.getBeanName() + "Controller.java"));
        Writer out = new OutputStreamWriter(outStream); // System.out
        // StringWriter out = new StringWriter();
        // System.out.println(out.toString());
        // 数据与模板合并（数据+模板=输出）
        template.process(root, out);
        out.flush();
        
        out.close();
    }
    
    /**
     * 生成Service
     * @param cfg
     * @param javaBean
     * @param savePath
     * @throws TemplateException
     * @throws IOException
     * @remark create 2016-11-23 baiguodong
     */
    private static void generService(Configuration cfg, JavaBean javaBean, String savePath) throws TemplateException, IOException
    {
        Template template = cfg.getTemplate("service.ftl");
        // 建立数据模型（Map）
        Map<String, Object> root = new HashMap<String, Object>();
        root.put("javaBean", javaBean);
        root.put("daTableName", javaBean.getDbName());
        String filePath = savePath + "\\service\\";
        File file = new File(filePath);
        if (!file.exists())
        {
            file.mkdirs();
        }
        
        // 获取输出流（指定到控制台（标准输出））
        OutputStream outStream = new FileOutputStream(new File(filePath + javaBean.getBeanName() + "Service.java"));
        Writer out = new OutputStreamWriter(outStream); // System.out
        // StringWriter out = new StringWriter();
        // System.out.println(out.toString());
        // 数据与模板合并（数据+模板=输出）
        template.process(root, out);
        out.flush();
        
        out.close();
    }
    
    /**
     * 生成DOMAIN
     * @param cfg
     * @param javaBean
     * @param savePath
     * @throws TemplateException
     * @throws IOException
     * @remark create 2016-11-23 baiguodong
     */
    private static void generDomain(Configuration cfg, JavaBean javaBean, String savePath) throws TemplateException, IOException
    {
        Template template = cfg.getTemplate("domain.ftl");
        // 建立数据模型（Map）
        Map<String, Object> root = new HashMap<String, Object>();
        root.put("javaBean", javaBean);
        root.put("daTableName", javaBean.getDbName());
        String filePath = savePath + "\\model\\domain\\";
        File file = new File(filePath);
        if (!file.exists())
        {
            file.mkdirs();
        }
        
        // 获取输出流（指定到控制台（标准输出））
        OutputStream outStream = new FileOutputStream(new File(filePath  + javaBean.getBeanName() + "Domain.java"));
        Writer out = new OutputStreamWriter(outStream); // System.out
        // StringWriter out = new StringWriter();
        // System.out.println(out.toString());
        // 数据与模板合并（数据+模板=输出）
        template.process(root, out);
        out.flush();
        
        out.close();
    }
    
    public static void main(String[] args) throws TemplateException, IOException
    {
        
    }
}
