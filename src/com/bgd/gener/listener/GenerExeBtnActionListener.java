package com.bgd.gener.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JComponent;
import javax.swing.JOptionPane;

import com.bgd.gener.exception.BlankStrException;
import com.bgd.gener.exception.DbDriverNotFoundException;
import com.bgd.gener.jdbc.DbOper;
import com.bgd.gener.view.DbInputPanel;

/**
 * 点击生成按钮监听器
 * @remark create 2016-11-21 baiguodong
 */
public class GenerExeBtnActionListener implements ActionListener
{
    private JComponent cmpnt;
    public GenerExeBtnActionListener(JComponent cmpnt)
    {
        this.cmpnt = cmpnt;
    }
    @Override
    public void actionPerformed(ActionEvent actionevent)
    {
        try 
        {
            DbOper.gener(((DbInputPanel)cmpnt).getBundle());
        }
        catch (BlankStrException e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
            return ;
        }
        catch (DbDriverNotFoundException e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
            return;
        }
        catch (SQLException sqlE)
        {
            JOptionPane.showMessageDialog(null, "数据库处理异常");
            return;
        }
        catch (Exception e1) {
            JOptionPane.showMessageDialog(null, e1.getMessage());
            return;
        }
    }
}
