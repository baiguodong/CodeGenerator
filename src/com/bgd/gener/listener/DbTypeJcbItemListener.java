package com.bgd.gener.listener;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import com.bgd.gener.jdbc.DbType;

/**
 * 数据库类型 下拉列表选择框
 * @remark create 2016-11-21 baiguodong
 */
public class DbTypeJcbItemListener implements ItemListener
{

    private JComboBox dbTypeJcb;
    public DbTypeJcbItemListener(JComboBox dbTypeJcb)
    {
        this.dbTypeJcb = dbTypeJcb;
    }
    
    @Override
    public void itemStateChanged(ItemEvent itemevent)
    {
        if (itemevent.getStateChange() == ItemEvent.SELECTED)
        {
            if (!itemevent.getItem().toString().equals(DbType.mysql.getType()))
            {
                JOptionPane.showMessageDialog(null, "敬请期待");
                int mySqlSelected = 0;
                dbTypeJcb.setSelectedIndex(mySqlSelected);
            }
        }
    }
}
