package com.bgd.gener;

import java.awt.EventQueue;

import javax.swing.JFrame;

import com.bgd.gener.view.GenerFrame;

/**
 * 代码自动生成工具类程序的主入口
 * 
 * @remark create 2016-11-18 baiguodong
 */
public class MainFrame
{
    public static void main(String[] args)
    {
        EventQueue.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                JFrame frame = new GenerFrame();
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setSize(500, 300);
                frame.setLocationRelativeTo(null);
                frame.setResizable(false);
                frame.setTitle("代码自动生成");
                frame.setVisible(true);
            }
        });
    }
}
