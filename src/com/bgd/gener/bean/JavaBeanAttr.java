package com.bgd.gener.bean;

import com.bgd.gener.jdbc.MySqlDataTypeTable;

/**
 * JavaBean定义
 * 
 * @author baiguodong
 * @remark create 2016-11-20 baiguodong
 */
public class JavaBeanAttr
{
    
    private String attrName;
    private String attrDoc;
    private String attrType;
    private String nameGetter;
    private String nameSetter;
    private String attrNameSharp;
    
    private JavaBeanAttr()
    {
        
    }
    
    
    /**
     * 
     * @param beanName
     * @param beanType
     * @return
     * @remark create 2016-11-20 baiguodong
     */
    /**
     * @param colName
     * @param colType
     * @return
     */
    public static final JavaBeanAttr getInstance(String colName, String colType, String attrDoc)
    {
        JavaBeanAttr javaBeanAttr = new JavaBeanAttr();
        javaBeanAttr.setAttrName(colName);
        javaBeanAttr.setAttrType(colType);
        javaBeanAttr.setAttrDoc(attrDoc);
        javaBeanAttr.setNameGetter(javaBeanAttr.getAttrName());
        javaBeanAttr.setNameSetter(javaBeanAttr.getAttrName());
        
        return javaBeanAttr;
    }
    
    public String getAttrName()
    {
        return attrName;
    }
    
    private void setAttrName(String colName)
    {
        if (colName != null)
        {
            // 以字母开头
            if (colName.charAt(0) >= 'a' && colName.charAt(0) >= 'Z')
            {
                if (colName.charAt(0) >= 'A' && colName.charAt(0) <= 'Z')
                {
                    colName = colName.replaceFirst(String.valueOf(colName.charAt(0)), String.valueOf(colName.charAt(0))
                            .toLowerCase());
                }
                else
                {
                    
                }
            }
            else
            {
                
            }
        }
        this.attrName = colName;
        
        this.attrNameSharp = "#{" + this.attrName + "}";
    }
    
    public String getAttrType()
    {
        return attrType;
    }
    
    private void setAttrType(String colType)
    {
        this.attrType = MySqlDataTypeTable.getJavaType(colType);
    }
    
    public String getNameGetter()
    {
        return nameGetter;
    }
    
    private void setNameGetter(String colName)
    {
        if (colName.charAt(0) >= 'a' && colName.charAt(0) <= 'z')
        {
            colName = colName
                    .replaceFirst(String.valueOf(colName.charAt(0)), String.valueOf(colName.charAt(0)).toUpperCase());
        }
        this.nameGetter = "get" + colName;
    }
    
    public String getNameSetter()
    {
        return this.nameSetter;
    }
    
    private void setNameSetter(String colName)
    {
        if (colName.charAt(0) >= 'a' && colName.charAt(0) <= 'z')
        {
            colName = colName
                    .replaceFirst(String.valueOf(colName.charAt(0)), String.valueOf(colName.charAt(0)).toUpperCase());
        }
        this.nameSetter = "set" + colName;
    }


    public String getAttrNameSharp()
    {
        return attrNameSharp;
    }


    public void setAttrNameSharp(String attrNameSharp)
    {
        this.attrNameSharp = attrNameSharp;
    }


    public String getAttrDoc()
    {
        return attrDoc;
    }


    public void setAttrDoc(String attrDoc)
    {
        this.attrDoc = attrDoc;
    }
}
