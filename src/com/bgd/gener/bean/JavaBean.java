package com.bgd.gener.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * java bean
 * 
 * @author baiguodong
 * @remark create 2016-11-20 baiguodong
 */
public class JavaBean
{
    private String dbName;
    private String beanName;
    private String classPath;
    private List<JavaBeanAttr> jbAttrs;
    private String aliasName;
    
    /**
     * 
     * @param dbName
     * @remark create 2016-11-25 baiguodong
     */
    public JavaBean(String dbName, String classPath)
    {
        jbAttrs = new ArrayList<JavaBeanAttr>();
        this.setDbName(dbName);
        this.setClassPath(classPath);
    }
    
    public String getBeanName()
    {
        return beanName;
    }
    
    private void setBeanName(String beanName)
    {
        if (beanName.contains("_"))
        {
            String[] beanNameParts = beanName.split("_");
            StringBuilder sb = new StringBuilder();
            for (String beanNamePart : beanNameParts)
            {
                if (beanNamePart.charAt(0) >= 'a' && beanNamePart.charAt(0) <= 'z')
                {
                    beanNamePart = beanNamePart.replaceFirst(String.valueOf(beanNamePart.charAt(0)), String.valueOf(beanNamePart.charAt(0)).toUpperCase());
                }
                sb.append(beanNamePart);
            }
            
            beanName = sb.toString();
        }
        else
        {
            if (beanName.charAt(0) >= 'a' && beanName.charAt(0) <= 'z')
            {
                beanName = beanName.replaceFirst(String.valueOf(beanName.charAt(0)), String.valueOf(beanName.charAt(0)).toUpperCase());
            }
        }
        this.beanName = beanName;
    }
    
    public List<JavaBeanAttr> getJbAttrs()
    {
        return jbAttrs;
    }
    
    public void setJbAttrs(List<JavaBeanAttr> jbAttrs)
    {
        this.jbAttrs = jbAttrs;
    }
    
    /** 类别名， 类名首字母小写 */
    public String getAliasName()
    {
//        aliasName = this.beanName.replaceFirst(String.valueOf(beanName.charAt(0)), String.valueOf(beanName.charAt(0)).toLowerCase());
        return aliasName;
    }

    /** 类别名， 类名首字母小写 */
    public void setAliasName(String aliasName)
    {
        aliasName = this.beanName.replaceFirst(String.valueOf(beanName.charAt(0)), String.valueOf(beanName.charAt(0)).toLowerCase());
        this.aliasName = aliasName;
    }

    /**
     * 添加java属性
     * 
     * @param jbAttr
     * @remark create 2016-11-20 baiguodong
     */
    public void addJavaBeanAttrs(JavaBeanAttr jbAttr)
    {
        this.jbAttrs.add(jbAttr);
    }

    public String getClassPath()
    {
        return classPath;
    }

    public void setClassPath(String classPath)
    {
        this.classPath = classPath;
    }

    public String getDbName()
    {
        return dbName;
    }

    /**
     * 设置数据库名称，并根据数据库名称生成相应的java类名和类别名
     * @param dbName
     * @remark create 2016-11-25 baiguodong
     */
    public void setDbName(String dbName)
    {
        this.dbName = dbName;
        this.setBeanName(dbName);
        this.setAliasName(this.getBeanName());
    }
    
    
}
