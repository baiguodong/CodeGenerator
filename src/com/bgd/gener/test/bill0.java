package com.bgd.gener.test;
public class bill0
{
        private int id;
        private String code;
        private String orderNo;
        private String createDate;
        private int typeId;
        private int transportModeId;
        private String buyer;
        private String receiver;
        private String address;
        private String phone;
        private int erpStatusId;
        private int allocateStatusId;
        private int pickStatusId;
        private int recheckStatusId;
        private int confirmStatusId;
        private int keyId;
        private String keyType;
        private int version;
        private int packStatusId;
        private int sendStatusId;
        private int billPositionId;
        private int billStatusId;
        private int sendBillToERPStatusId;
    
        private int getId()
        {
           return this.id;
        }
        
        private void setId(int id)
        {
           this.id = id;
        }
        private String getCode()
        {
           return this.code;
        }
        
        private void setCode(String code)
        {
           this.code = code;
        }
        private String getOrderNo()
        {
           return this.orderNo;
        }
        
        private void setOrderNo(String orderNo)
        {
           this.orderNo = orderNo;
        }
        private String getCreateDate()
        {
           return this.createDate;
        }
        
        private void setCreateDate(String createDate)
        {
           this.createDate = createDate;
        }
        private int getTypeId()
        {
           return this.typeId;
        }
        
        private void setTypeId(int typeId)
        {
           this.typeId = typeId;
        }
        private int getTransportModeId()
        {
           return this.transportModeId;
        }
        
        private void setTransportModeId(int transportModeId)
        {
           this.transportModeId = transportModeId;
        }
        private String getBuyer()
        {
           return this.buyer;
        }
        
        private void setBuyer(String buyer)
        {
           this.buyer = buyer;
        }
        private String getReceiver()
        {
           return this.receiver;
        }
        
        private void setReceiver(String receiver)
        {
           this.receiver = receiver;
        }
        private String getAddress()
        {
           return this.address;
        }
        
        private void setAddress(String address)
        {
           this.address = address;
        }
        private String getPhone()
        {
           return this.phone;
        }
        
        private void setPhone(String phone)
        {
           this.phone = phone;
        }
        private int getErpStatusId()
        {
           return this.erpStatusId;
        }
        
        private void setErpStatusId(int erpStatusId)
        {
           this.erpStatusId = erpStatusId;
        }
        private int getAllocateStatusId()
        {
           return this.allocateStatusId;
        }
        
        private void setAllocateStatusId(int allocateStatusId)
        {
           this.allocateStatusId = allocateStatusId;
        }
        private int getPickStatusId()
        {
           return this.pickStatusId;
        }
        
        private void setPickStatusId(int pickStatusId)
        {
           this.pickStatusId = pickStatusId;
        }
        private int getRecheckStatusId()
        {
           return this.recheckStatusId;
        }
        
        private void setRecheckStatusId(int recheckStatusId)
        {
           this.recheckStatusId = recheckStatusId;
        }
        private int getConfirmStatusId()
        {
           return this.confirmStatusId;
        }
        
        private void setConfirmStatusId(int confirmStatusId)
        {
           this.confirmStatusId = confirmStatusId;
        }
        private int getKeyId()
        {
           return this.keyId;
        }
        
        private void setKeyId(int keyId)
        {
           this.keyId = keyId;
        }
        private String getKeyType()
        {
           return this.keyType;
        }
        
        private void setKeyType(String keyType)
        {
           this.keyType = keyType;
        }
        private int getVersion()
        {
           return this.version;
        }
        
        private void setVersion(int version)
        {
           this.version = version;
        }
        private int getPackStatusId()
        {
           return this.packStatusId;
        }
        
        private void setPackStatusId(int packStatusId)
        {
           this.packStatusId = packStatusId;
        }
        private int getSendStatusId()
        {
           return this.sendStatusId;
        }
        
        private void setSendStatusId(int sendStatusId)
        {
           this.sendStatusId = sendStatusId;
        }
        private int getBillPositionId()
        {
           return this.billPositionId;
        }
        
        private void setBillPositionId(int billPositionId)
        {
           this.billPositionId = billPositionId;
        }
        private int getBillStatusId()
        {
           return this.billStatusId;
        }
        
        private void setBillStatusId(int billStatusId)
        {
           this.billStatusId = billStatusId;
        }
        private int getSendBillToERPStatusId()
        {
           return this.sendBillToERPStatusId;
        }
        
        private void setSendBillToERPStatusId(int sendBillToERPStatusId)
        {
           this.sendBillToERPStatusId = sendBillToERPStatusId;
        }
}