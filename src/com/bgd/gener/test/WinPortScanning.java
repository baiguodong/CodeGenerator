package com.bgd.gener.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author Administrator
 * @remark create 2016-11-24 baiguodong
 */
public class WinPortScanning
{
    public static void main(String[] args) throws IOException
    {
        Runtime rt = Runtime.getRuntime();
        Process process = rt.exec("netstat -aon");
        InputStream in = process.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in, "GBK"));
        int i = 0;
        String temp = null;
        StringBuilder sb = new StringBuilder();
        while ((temp = reader.readLine()) != null)
        {
            sb.append(temp);
            if (++i > 4)
            {
                if (temp.contains("LISTENING") && temp.contains("TCP"))
                {
                   System.out.println(temp.substring(temp.lastIndexOf(" ") + 1));
                }
            }
        }
        
//        System.out.println(sb.toString());
    }
}
