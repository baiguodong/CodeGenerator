package com.bgd.gener.jdbc;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bgd.gener.bean.JavaBean;
import com.bgd.gener.bean.JavaBeanAttr;
import com.bgd.gener.bundles.DbInputValueBundle;
import com.bgd.gener.exception.DbDriverNotFoundException;

/**
 * MySql 数据库操作类
 * @remark create 2016-11-20 baiguodong
 */
public class MySqlOper
{
	
	private DbInputValueBundle bundle;
	public MySqlOper(DbInputValueBundle bundle) throws SQLException
	{
		this.bundle = bundle;
	}
	 /**
	  * @see http://www.cnblogs.com/lbangel/p/3487796.html
	  * @return
	 * @throws DbDriverNotFoundException 
	 * @throws SQLException 
	  */
   private Connection getConnection() throws DbDriverNotFoundException, SQLException 
   {
	    String driver = "com.mysql.jdbc.Driver";
//	    String url = "jdbc:mysql://123.233.247.3:3306/wmsdev?useUnicode=true&amp;characterEncoding=utf8";
	    String url = bundle.getDbUrl() + "?useUnicode=true&amp;characterEncoding=utf8";
	    String username = bundle.getDbUsername();
	    String password = bundle.getDbPassword();
	    Connection conn = null;
	    try {
	        Class.forName(driver); //classLoader,加载对应驱动
	        conn = (Connection) DriverManager.getConnection(url, username, password);
	    } catch (ClassNotFoundException e) {
	        throw new DbDriverNotFoundException("数据库驱动加载异常");
	    } catch (SQLException e) {
	        throw new SQLException("数据库连接异常");
	    }
	    return conn;
   }
   
   /**
    * @see http://www.cnblogs.com/lbangel/p/3487796.html
    * @param args
    * @throws SQLException
 * @throws DbDriverNotFoundException 
    */
   public List<JavaBean> gener() throws SQLException, DbDriverNotFoundException
   {
       if (this.getConnection() != null) 
       {
       	System.out.println("获取数据库连接成功");
       }
       
       Connection conn = this.getConnection();
       DatabaseMetaData m_DBMetaData = conn.getMetaData(); 
       List<JavaBean> javaBeans = new ArrayList<JavaBean>();
       String []dbTables = bundle.getDbTable();
       for (String dbTable : dbTables)
       {
           // 获取数据库中所有的表的名字，第二个% 可以指定为具体表的名字
           ResultSet tableRet = m_DBMetaData.getTables(null, "%", dbTable, new String[]{"TABLE"}); 
           while (tableRet.next()) 
           {
        //          System.out.println(tableRet.getString("TABLE_NAME"));
                JavaBean javaBean = new JavaBean(tableRet.getString("TABLE_NAME"), bundle.getClassPath());
                // 获取指定数据库表的字段
                ResultSet colRet = m_DBMetaData.getColumns(null, "%", tableRet.getString("TABLE_NAME"), "%"); 
                while(colRet.next()) { 
                    String columnName = colRet.getString("COLUMN_NAME"); 
                    String columnType = colRet.getString("TYPE_NAME");
                    String comment = colRet.getString("REMARKS");
//                    int datasize = colRet.getInt("COLUMN_SIZE"); 
//                    int digits = colRet.getInt("DECIMAL_DIGITS"); 
//                    int nullable = colRet.getInt("NULLABLE"); 
        //              System.out.println(columnName+" "+columnType+" "+datasize+" "+digits+" "+ nullable); 
                    
                    javaBean.addJavaBeanAttrs(JavaBeanAttr.getInstance(columnName, columnType, comment));
                }
                
                javaBeans.add(javaBean);
           }
       }
       return javaBeans;
   }
}
