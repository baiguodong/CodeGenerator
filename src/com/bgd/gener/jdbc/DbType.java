package com.bgd.gener.jdbc;

/**
 * 数据库类型
 * 
 * @remark create 2016-11-20 baiguodong
 */
public enum DbType
{
    mysql("mysql"), oracle("oracle"), db2("db2");
    
    private String type;
    
    private DbType(String type)
    {
        this.type = type;
    }
    
    public String getType()
    {
        return type;
    }
    
    public void setType(String type)
    {
        this.type = type;
    }
    
}
