package com.bgd.gener.jdbc;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.bgd.gener.bean.JavaBean;
import com.bgd.gener.bundles.DbInputValueBundle;
import com.bgd.gener.exception.DbDriverNotFoundException;
import com.bgd.gener.freemarker.FreeMarkerUtils;

import freemarker.template.TemplateException;

/**
 * 数据库操作类
 * @author baiguodong
 *  @see http://www.cnblogs.com/lbangel/p/3487796.html
 * @remark create 2016-11-18 baiguodong
 */
public class DbOper 
{
	public final static void gener(DbInputValueBundle bundle) throws SQLException, TemplateException, IOException, DbDriverNotFoundException
	{
		DbType type = bundle.getDbType();
		if (type == DbType.mysql)
		{
			MySqlOper mySqlOper = new MySqlOper(bundle);
			List<JavaBean> javaBeans = mySqlOper.gener();
			FreeMarkerUtils.out(javaBeans, bundle.getSavePath());
		}
	}
	
	 /**
	  * @see http://www.cnblogs.com/lbangel/p/3487796.html
	  * @return
	  */
    private static Connection getConnection() 
    {
	    String driver = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://123.233.247.3:3306/wmsdev?useUnicode=true&amp;characterEncoding=utf8";
	    String username = "wms";
	    String password = "wms@meixian";
	    Connection conn = null;
	    try {
	        Class.forName(driver); //classLoader,加载对应驱动
	        conn = (Connection) DriverManager.getConnection(url, username, password);
	    } catch (ClassNotFoundException e) {
	        e.printStackTrace();
	    } catch (SQLException e) {
	        e.printStackTrace();
	    }
	    return conn;
    }
    
    /**
     * @see http://www.cnblogs.com/lbangel/p/3487796.html
     * @param args
     * @throws SQLException
     */
    public static void main(String[] args) throws SQLException
    {
        if (DbOper.getConnection() != null) 
        {
        	System.out.println("获取数据库连接成功");
        }
        
        Connection conn = DbOper.getConnection();
        DatabaseMetaData m_DBMetaData = conn.getMetaData(); 
        // 获取数据库中所有的表的名字，第二个% 可以指定为具体表的名字
        ResultSet tableRet = m_DBMetaData.getTables(null, "%", "%", new String[]{"TABLE"}); 
        
        while (tableRet.next()) 
        {
        	System.out.println(tableRet.getString("TABLE_NAME"));
        	
        	// 获取指定数据库表的字段
        	ResultSet colRet = m_DBMetaData.getColumns(null,"%", tableRet.getString("TABLE_NAME"),"%"); 
        	while(colRet.next()) { 
        	   String columnName = colRet.getString("COLUMN_NAME"); 
        	   String columnType = colRet.getString("TYPE_NAME"); 
        	   int datasize = colRet.getInt("COLUMN_SIZE"); 
        	   int digits = colRet.getInt("DECIMAL_DIGITS"); 
        	   int nullable = colRet.getInt("NULLABLE"); 
        	   System.out.println(columnName+" "+columnType+" "+datasize+" "+digits+" "+ nullable); 
        	}
        	System.out.println("-------------------------------------");
        }
    }
}
