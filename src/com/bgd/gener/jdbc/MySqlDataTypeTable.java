package com.bgd.gener.jdbc;

import java.util.HashMap;
import java.util.Map;

/**
 * MySql数据库与java对象的映射关系
 * @author baiguodong
 * remark create 2016-11-20 baiguodong
 */
public class MySqlDataTypeTable 
{
	private static final Map<String, String> dataTypeMap = new HashMap<String, String>();
	static
	{
		dataTypeMap.put("INT", "int");
	    dataTypeMap.put("VARCHAR", "String");
	}
	
	public static final String getJavaType(String dbDataType)
	{
		return dataTypeMap.get(dbDataType);
	}
}
