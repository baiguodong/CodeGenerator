package com.bgd.gener.bundles;

import com.bgd.gener.jdbc.DbType;

/**
 * 数据库配置信息数据交换类
 * 
 * @remark create 2016-11-20 baiguodong
 */
public class DbInputValueBundle
{
    private DbType dbType;
    private String dbUrl;
    private String dbUsername;
    private String dbPassword;
    private String[] dbTable;
    private String classPath;
    private String savePath;
    
    public DbType getDbType()
    {
        return dbType;
    }
    
    public void setDbType(DbType dbType)
    {
        this.dbType = dbType;
    }
    
    public String getDbUrl()
    {
        return dbUrl;
    }
    
    public void setDbUrl(String dbUrl)
    {
        this.dbUrl = dbUrl;
    }
    
    public String getDbUsername()
    {
        return dbUsername;
    }
    
    public void setDbUsername(String dbUsername)
    {
        this.dbUsername = dbUsername;
    }
    
    public String getDbPassword()
    {
        return dbPassword;
    }
    
    public void setDbPassword(String dbPassword)
    {
        this.dbPassword = dbPassword;
    }
    
    public String[] getDbTable()
    {
        return dbTable;
    }
    
    public void setDbTable(String[] dbTable)
    {
        this.dbTable = dbTable;
    }
    
    public String getClassPath()
    {
        return classPath;
    }

    public void setClassPath(String classPath)
    {
        this.classPath = classPath;
    }

    public String getSavePath()
    {
        return savePath;
    }
    
    public void setSavePath(String savePath)
    {
        this.savePath = savePath;
    }
    
}
