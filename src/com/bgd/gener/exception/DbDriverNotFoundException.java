package com.bgd.gener.exception;

/**
 * 数据库驱动异常
 * @remark create 2016-11-21 baiguodong
 */
public class DbDriverNotFoundException extends Exception
{
    public DbDriverNotFoundException(String msg)
    {
        super(msg);
    }
    
    public DbDriverNotFoundException(Exception e)
    {
        super(e);
    }
    
    public DbDriverNotFoundException(String msg, Exception e)
    {
        super(msg, e);
    }
}
