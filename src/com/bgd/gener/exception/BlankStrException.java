package com.bgd.gener.exception;

/**
 * 空白字符串异常， 包括null, "" "  "等字符串形式
 * @remark create 2016-11-21 baiguodong
 */
public class BlankStrException extends Exception
{
    public BlankStrException(String msg)
    {
        super(msg);
    }
    
    public BlankStrException(Exception e)
    {
        super(e);
    }
    
    public BlankStrException(String msg, Exception e)
    {
        super(msg, e);
    }
}
