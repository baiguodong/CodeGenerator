package com.bgd.gener.view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.bgd.gener.exception.BlankStrException;
import com.bgd.gener.jdbc.DbOper;
import com.bgd.gener.listener.GenerExeBtnActionListener;

/**
 * 生成按钮Panel
 * @remark create 2016-11-18 baiguodong
 */
public class GenerExePanel extends JPanel
{
	private DbInputLabelPanel dbInputLabPal;
    private DbInputPanel dbInputPal;
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GenerExePanel()
    {
        super(new BorderLayout());
        this.init();
    }
    
    public GenerExePanel(LayoutManager layoutManager)
    {
        super(layoutManager);
        this.init();
    }
    
    public void init()
    {
    	dbInputLabPal = new DbInputLabelPanel(new GridLayout(7, 1));
    	dbInputPal = new DbInputPanel(new GridLayout(7, 1));
        JButton generExeBtn = new JButton("生成");
        
        this.add(dbInputLabPal, BorderLayout.WEST);
        this.add(dbInputPal, BorderLayout.CENTER);
        this.add(generExeBtn, BorderLayout.SOUTH);
        
        generExeBtn.addActionListener(new GenerExeBtnActionListener(dbInputPal));
    }
}
