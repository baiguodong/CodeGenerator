package com.bgd.gener.view;

import java.awt.LayoutManager;

import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * 数据库数据输入信息描述的Label选项
 * @remark create 2016-11-20 baiguodong
 */
public class DbInputLabelPanel extends JPanel
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DbInputLabelPanel()
    {
    	super();
    	this.init();
    }
    
    public DbInputLabelPanel(LayoutManager layoutManager)
    {
    	super(layoutManager);
    	this.init();
    }
    
    private void init()
    {
    	JLabel dbTypeLab = new JLabel("数据库类型：");
    	JLabel dbUrlLab = new JLabel("数据库地址:");
    	JLabel dbUserNameLab = new JLabel("用户名：");
    	JLabel dbPasswordLab = new JLabel("密码：");
    	JLabel dbTableLab = new JLabel("表名：");
        JLabel dbClassPath = new JLabel("类名：");
    	JLabel pathLab = new JLabel("保存路径：");
    	
    	this.add(dbTypeLab);
    	this.add(dbUrlLab);
    	this.add(dbUserNameLab);
    	this.add(dbPasswordLab);
    	this.add(dbTableLab);
    	this.add(dbClassPath);
    	this.add(pathLab);
    }
}
