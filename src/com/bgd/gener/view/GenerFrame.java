package com.bgd.gener.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * 代码自动生成工具的主页面
 * 
 * @remark create 2016-11-18 baiguodong
 */
public class GenerFrame extends JFrame
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GenerFrame()
    {
        // 设置布局Frame布局管理器
        this.setLayout(new BorderLayout());
        GenerExePanel generExePanel = new GenerExePanel();
        
        this.add(generExePanel, BorderLayout.CENTER);
    }
}
