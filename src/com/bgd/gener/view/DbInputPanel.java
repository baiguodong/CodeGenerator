package com.bgd.gener.view;

import java.awt.LayoutManager;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.bgd.gener.bundles.DbInputValueBundle;
import com.bgd.gener.exception.BlankStrException;
import com.bgd.gener.jdbc.DbType;
import com.bgd.gener.listener.DbTypeJcbItemListener;

/**
 * 数据库信息输入框Panel
 * 
 * @remark create 2016-11-20 baiguodong
 */
public class DbInputPanel extends JPanel
{
    private JComboBox dbTypeJcb;
    private JTextField dbUrlJtg;
    private JTextField dbUserNameJtg;
    private JTextField dbPasswordJtg;
    private JTextField dbTableJtf;
    private JTextField classPathJtf;
    private JTextField pathJtf;
    
    private DbInputValueBundle bundle;
    
    public DbInputPanel()
    {
        super();
        this.init();
    }
    
    public DbInputPanel(LayoutManager layoutManager)
    {
        super(layoutManager);
        this.init();
    }
    
    private void init()
    {
        // 数据库类型
        dbTypeJcb = new JComboBox();
        dbTypeJcb.setModel(new DefaultComboBoxModel(new String[] { DbType.mysql.getType(), DbType.oracle
                .getType(), DbType.db2.getType() }));
        
        // 数据库地址
        dbUrlJtg = new JTextField(30);
        // 用户名
        dbUserNameJtg = new JTextField(30);
        // 密码
        dbPasswordJtg = new JTextField(30);
        // 表名
        dbTableJtf = new JTextField(30);
        classPathJtf = new JTextField(30);
        // 生成文件保存路径
        pathJtf = new JTextField(30);
        
        this.add(dbTypeJcb);
        this.add(dbUrlJtg);
        this.add(dbUserNameJtg);
        this.add(dbPasswordJtg);
        this.add(dbTableJtf);
        this.add(classPathJtf);
        this.add(pathJtf);
        
        dbTypeJcb.addItemListener(new DbTypeJcbItemListener(dbTypeJcb));
    }
    
    /**
     * 组织数据库交换数据
     * 
     * @return
     * @throws Exception
     * @remark create 2016-11-20 baiguodong
     */
    public DbInputValueBundle getBundle() throws Exception
    {
        bundle = new DbInputValueBundle();
        bundle.setDbType(this.getDbType());
        bundle.setDbUrl(this.getDbUrl());
        bundle.setDbUsername(this.getDbUsername());
        bundle.setDbPassword(this.getDbPassword());
        bundle.setDbTable(this.getDbTable());
        bundle.setClassPath(this.getClassPath());
        bundle.setSavePath(this.getSavePath());
        
        return bundle;
    }
    
    /**
     * 返回选中的数据库类型，默认是MySql
     * 
     * @param dbType
     * @return
     * @remark create 2016-11-20 baiguodong
     */
    private DbType getDbType()
    {
        String dbType = this.getDbTypeJcb().getSelectedItem().toString();
        if (dbType.equals(DbType.mysql.getType()))
        {
            return DbType.mysql;
        }
        
        if (dbType.equals(DbType.oracle.getType()))
        {
            return DbType.oracle;
        }
        
        if (dbType.equals(DbType.oracle.getType()))
        {
            return DbType.db2;
        }
        
        return DbType.mysql;
    }
    
    /**
     * 获取数据库连接地址
     * 
     * @return
     * @throws Exception
     * @remark create 2016-11-20 baiguodong
     */
    private String getDbUrl() throws BlankStrException
    {
        String dbUrl = this.getDbUrlJtg().getText().trim();
        if ("".equals(dbUrl))
        {
            throw new BlankStrException("数据库连接地址不能为空");
        }
        
        return dbUrl;
    }
    
    /**
     * 获取数据库的用户名
     * 
     * @return
     * @throws Exception
     * @remark create 2016-11-20 baiguodong
     */
    private String getDbUsername() throws BlankStrException
    {
        String dbUsername = this.getDbUserNameJtg().getText().trim();
        if ("".equals(dbUsername))
        {
            throw new BlankStrException("数据库用户名不能为空");
        }
        
        return dbUsername;
    }
    
    /**
     * 数据库密码不能为空
     * 
     * @return
     * @throws Exception
     * @remark create 2016-11-20 baiguodong
     */
    private String getDbPassword() throws BlankStrException
    {
        String dbPassword = this.getDbPasswordJtg().getText().trim();
        if ("".equals(dbPassword))
        {
            throw new BlankStrException("数据库密码不能为空");
        }
        
        return dbPassword;
    }
    
    /**
     * 数据库表不能为空
     * 
     * @return
     * @throws Exception
     * @remark create 2016-11-20 baiguodong
     */
    private String[] getDbTable() throws BlankStrException
    {
        String dbTable = this.getDbTableJtf().getText().trim();
        if ("".equals(dbTable) && !dbTable.endsWith(";"))
        {
            throw new BlankStrException("数据库表不能为空");
        }
        
        return dbTable.split(";");
    }
    
    /**
     * 获取类路径
     * @return
     * @throws BlankStrException
     * @remark create 2016-11-21 baiguodong
     */
    private String getClassPath() throws BlankStrException
    {
        String classPath = this.getClassPathJtf().getText().trim();
        if ("".equals(classPath))
        {
            throw new BlankStrException("类路径不能为空");
        }
        
        return classPath;
    }
    
    /**
     * 获取生成文件保存路径
     * 
     * @return
     * @throws Exception
     * @remark create 2016-11-20 baiguodong
     */
    private String getSavePath() throws BlankStrException, Exception
    {
        String savePath = this.getPathJtf().getText().trim();
        if ("".equals(savePath))
        {
            throw new BlankStrException("保存路径不能为空");
        }
        
        File file = new File(savePath);
        if (!file.exists())
        {
            try
            {
                file.mkdirs();
            }
            catch (Exception e)
            {
                throw new Exception("创建目录失败");
            }
            if (!file.isDirectory())
            {
                throw new Exception("当前输入路径为非目录结构");
            }
        }
        return savePath;
    }
    
    public void setBundle(DbInputValueBundle bundle)
    {
        this.bundle = bundle;
    }
    
    private JComboBox getDbTypeJcb()
    {
        return dbTypeJcb;
    }
    
    private void setDbTypeJcb(JComboBox dbTypeJcb)
    {
        this.dbTypeJcb = dbTypeJcb;
    }
    
    private JTextField getDbUrlJtg()
    {
        return dbUrlJtg;
    }
    
    private void setDbUrlJtg(JTextField dbUrlJtg)
    {
        this.dbUrlJtg = dbUrlJtg;
    }
    
    private JTextField getDbUserNameJtg()
    {
        return dbUserNameJtg;
    }
    
    private void setDbUserNameJtg(JTextField dbUserNameJtg)
    {
        this.dbUserNameJtg = dbUserNameJtg;
    }
    
    private JTextField getDbPasswordJtg()
    {
        return dbPasswordJtg;
    }
    
    private void setDbPasswordJtg(JTextField dbPasswordJtg)
    {
        this.dbPasswordJtg = dbPasswordJtg;
    }
    
    private JTextField getDbTableJtf()
    {
        return dbTableJtf;
    }
    
    private void setDbTableJtf(JTextField dbTableJtf)
    {
        this.dbTableJtf = dbTableJtf;
    }
    
    public JTextField getClassPathJtf()
    {
        return classPathJtf;
    }

    public void setClassPathJtf(JTextField classPathJtf)
    {
        this.classPathJtf = classPathJtf;
    }

    private JTextField getPathJtf()
    {
        return pathJtf;
    }
    
    private void setPathJtf(JTextField pathJtf)
    {
        this.pathJtf = pathJtf;
    }
    
}
