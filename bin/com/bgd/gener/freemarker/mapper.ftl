<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" 
"http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<!-- 
	namespace：必须与对应的接口全类名一致
	id:必须与对应接口的某个对应的方法名一致
	
	 @remark create by CodeGenerator at ${.now}
	
	将一下字符串剪贴到mybatis总的配置文件中：
	<mapper resource="${javaBean.classPath?replace('.', '/' )}/dao/${javaBean.beanName}Dao.xml" />
	<typeAlias alias="${javaBean.aliasName}" type="${javaBean.classPath}.model.${javaBean.beanName}" />
 -->
<mapper namespace="${javaBean.classPath}.dao.${javaBean.beanName}Dao">
	
	<insert id="save${javaBean.beanName}" parameterType="${javaBean.aliasName}">
		insert into ${dbTableName} (
		 <#list javaBean.jbAttrs as jbAttr>
		    ${jbAttr.attrName}<#if jbAttr_has_next>,</#if>
		 </#list>
		 ) values(
		 <#list javaBean.jbAttrs as jbAttr>
		    ${jbAttr.attrNameSharp}<#if jbAttr_has_next>,</#if>
		 </#list>
		 )
	</insert>
	
	<update id="update${javaBean.beanName}" parameterType="${javaBean.aliasName}">
		update ${dbTableName} set 
		<#list javaBean.jbAttrs as jbAttr>
		    <#if jbAttr.attrName != 'id'>
		     ${jbAttr.attrName}= ${jbAttr.attrNameSharp}<#if jbAttr_has_next>,</#if>
		    </#if>
		</#list>
		where id=<#list javaBean.jbAttrs as jbAttr><#if jbAttr.attrName == 'id'>${jbAttr.attrNameSharp}</#if></#list>
	</update>
	
	<delete id="delete${javaBean.beanName}" parameterType="int">
		delete from ${dbTableName} where id= <#list javaBean.jbAttrs as jbAttr> <#if jbAttr.attrName == 'id'>${jbAttr.attrNameSharp}</#if></#list>
	</delete>
	
	 <!-- mybsits_config中配置的alias类别名,也可直接配置resultType为类路劲 -->  
	<select id="find${javaBean.beanName}ById" parameterType="int" resultType="${javaBean.aliasName}">
		select
		<#list javaBean.jbAttrs as jbAttr>
		    ${jbAttr.attrName}<#if jbAttr_has_next>,</#if>
		</#list>
		from 
		${dbTableName} 
		where id=<#list javaBean.jbAttrs as jbAttr> <#if jbAttr.attrName == 'id'>${jbAttr.attrNameSharp}</#if></#list>
	</select>
	
	<select id="find${javaBean.beanName}All" resultType="${javaBean.aliasName}">
		select
		<#list javaBean.jbAttrs as jbAttr>
		    ${jbAttr.attrName}<#if jbAttr_has_next>,</#if>
		</#list>
		from 
		${dbTableName} 
	</select>
	
	<select id="find${javaBean.beanName}ByPage" resultType="${javaBean.aliasName}">
		select
		<#list javaBean.jbAttrs as jbAttr>
		    ${jbAttr.attrName}<#if jbAttr_has_next>,</#if>
		</#list>
		from 
		${dbTableName} limit 0,20
	</select>
</mapper>