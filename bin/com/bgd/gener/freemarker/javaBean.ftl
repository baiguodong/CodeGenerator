package ${javaBean.classPath}.model;

import  ${javaBean.classPath}.model.domain.${javaBean.beanName}Domain;
import org.springframework.beans.factory.annotation.Autowired;
/**
 * @remark create by CodeGenerator at ${.now}
 */

public class ${javaBean.beanName}
{
    
     @Autowired
     private ${javaBean.beanName}Domain ${javaBean.aliasName}Domain;
    /*--------------- 与数据库表字段对应的类属性及其getter\setter  -----------------------*/
    <#list javaBean.jbAttrs as jbAttr>
        /** ${jbAttr.attrDoc} */
        public static final String COL_${jbAttr.attrName} = "${jbAttr.attrName}";
    </#list>
    
    <#list javaBean.jbAttrs as jbAttr>
        /** ${jbAttr.attrDoc} */
        private ${jbAttr.attrType} ${jbAttr.attrName};
    </#list>
    
     <#list javaBean.jbAttrs as jbAttr>
        /** 获取${jbAttr.attrDoc} */
        public ${jbAttr.attrType} ${jbAttr.nameGetter}()
        {
           return this.${jbAttr.attrName};
        }
        
        /** 设置${jbAttr.attrDoc} */
        public void ${jbAttr.nameSetter}(${jbAttr.attrType} ${jbAttr.attrName})
        {
           this.${jbAttr.attrName} = ${jbAttr.attrName};
        }
    </#list>
    /*--------------- 与数据库表字段对应的类属性及其getter\setter  -----------------------*/
}