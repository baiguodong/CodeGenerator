package ${javaBean.classPath}.dao;

import ${javaBean.classPath}.model.${javaBean.beanName};
import java.util.List;

/**
 * @remark create by CodeGenerator at ${.now}
 */

public interface ${javaBean.beanName}Dao
{
    /** 
     * 保存
     * @param ${javaBean.beanName} 
     * @remark create by CodeGenerator
     */
    void save${javaBean.beanName}(${javaBean.beanName} ${javaBean.aliasName});
    
    /** 
     * 更新
     * @param ${javaBean.beanName} 
     * @return boolean
     * @remark create by CodeGenerator
     */
	boolean update${javaBean.beanName}(${javaBean.beanName} ${javaBean.aliasName});
	
	/** 
     * 删除
     * @param id
     * @return boolean
     * @remark create by CodeGenerator
     */
	boolean delete${javaBean.beanName}(int id);
	
	/** 
     * 根据ID返回
     * @param id
     * @return ${javaBean.beanName} 
     * @remark create by CodeGenerator
     */
	${javaBean.beanName} find${javaBean.beanName}ById(int id);
	/** 
     * 获取所有的
     * @return List<${javaBean.beanName}>
     * @remark create by CodeGenerator
     */
	List<${javaBean.beanName}> find${javaBean.beanName}All(); 
}